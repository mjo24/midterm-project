"""
Script to use with MoveIt and gazebo to draw initial on a board. This script
was inspired by the MoveIt tutorial (posted on Ed Discussion) with significant edits

Author: Marcus Ortiz
"""
from __future__ import print_function

import copy
import time
from typing import Optional, Tuple, List
from math import tau
import numpy as np
import sys
import rospy
import moveit_commander
import geometry_msgs.msg

"""
Description

Code Structure
    
"""


class DrawingRobot(object):
    """ Robot that can draw items on a board using either Poses or Cartesian Paths for EE
    Drawable Items:
        - M
        - J
        - O
    """

    def __init__(self):
        super(DrawingRobot, self).__init__()

        # Initialize moveit_commander and rospy
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        # Initialize RobotCommander to get information about robot
        robot = moveit_commander.RobotCommander()

        # Initialize MoveGroupCommander to plan and execute motions for manipulator (URD5)
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        # Minimize Speed
        move_group.set_max_velocity_scaling_factor(0.5)  # Set speed to 50% of the maximum velocity

        # rospy.set_param("/move_group/start_state_max_bounds_error", 0.1)

        # Print robot state
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        # Set class variables for reference
        self.robot = robot
        self.move_group = move_group
        self.boardDistance = .55

    def curve_poses(self, center: (float, float), r, startTheta, endTheta, step) -> List[Tuple[float, float, float]]:
        """ Generate points on a circle.

        Arguments:
            center {tuple} -- (x, z) center of curve
            r {float} -- radius of curve
            startTheta {float} -- starting angle of curve
            endTheta {float} -- ending angle of curve
            step {float} -- angle step size of curve
        """

        theta = np.linspace(startTheta * np.pi / 180, endTheta * np.pi / 180, np.abs(endTheta - startTheta) // step)
        x = center[0] + r * np.cos(theta)
        y = self.boardDistance * np.ones(len(theta))
        z = center[1] + r * np.sin(theta)
        return list(zip(x, y, z))

    def go_to_joint_state(self, base, shoulder, elbow, wristOne, wristTwo, wristThree):
        """ Plans and then moves the robot to the specified joint state

        Arguments:
            base {float} -- Base joint angle
            shoulder {float} -- Shoulder joint angle
            elbow {float} -- Elbow joint angle
            wristOne {float} -- Wrist One joint angle
            wristTwo {float} -- Wrist Two joint angle
            wristThree {float} -- Wrist Three joint angle
        """
        # Planning joint states
        joint_goal = self.move_group.get_current_joint_values()
        joint_goal[0] = base
        joint_goal[1] = shoulder
        joint_goal[2] = elbow
        joint_goal[3] = wristOne
        joint_goal[4] = wristTwo
        joint_goal[5] = wristThree

        # Execute and stop movement
        self.move_group.go(joint_goal, wait=True)
        self.move_group.stop()

    def go_to_pose_goal(self, pos: (float, float, float),
                        ori: Optional[Tuple[float, float, float]]):
        """ Plans and then moves the end effector to the specified pose

        Arguments:
            position {tuple} -- (x, y, z) position of end effector
            orientation {tuple} -- (w, x, y, z) orientation of end effector, defaults to current orientation
        """

        # If no orientation is specified, use current orientation
        if not ori:
            current_orientation = self.move_group.get_current_pose().pose.orientation
            ori = (current_orientation.w, current_orientation.x, current_orientation.y, current_orientation.z)

        # Planning final EE position
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal_pos = pose_goal.position
        pose_goal_pos.x, pose_goal_pos.y, pose_goal_pos.z = pos[0], pos[1], pos[2]  # Planning position
        pose_goal_ori = pose_goal.orientation
        pose_goal_ori.w, pose_goal_ori.x, pose_goal_ori.y, pose_goal_ori.z = ori[0], ori[1], ori[2], ori[
            3]  # Planning orientation
        self.move_group.set_pose_target(pose_goal)

        # Plan and execute pose
        success = self.move_group.go(wait=True)
        self.move_group.stop()

        # Clear saved pose
        self.move_group.clear_pose_targets()

    def plan_cartesian_path(self, points: List[Tuple[float, float, float]]):
        """ Plans cartesian path by creating waypoints from a list of points

        Arguments:
            points {list} -- list of points (x, y, z) to create waypoints from
        """

        waypoints = []
        # Loops through points and add each as waypoint
        for point in points:
            wpose = self.move_group.get_current_pose().pose
            wpose.position.x = point[0]
            wpose.position.y = point[1]
            wpose.position.z = point[2]
            waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = self.move_group.compute_cartesian_path(
            waypoints, 0.005, 0.0
        )
        return plan, fraction

    def startPosition(self):
        """ Moves the robot to the starting position with joint goals
        """
        self.go_to_joint_state(0, -tau / 14, tau / 18, -tau / 2, 0, 0)

    def drawM(self, bottomLeft):
        """ Draws an M using cartesian path of points, ends with pen up
        using a pose goal
        """
        # Define positions for each point of the M
        x, y, z = bottomLeft
        self.go_to_pose_goal((x, y-.1, z), None)
        upperLeft, middle, upperRight, bottomRight = (
            (x, y, z + .3), (x + .15, y, z + .1), (x + .3, y, z + .3), (x + .3, y, z))
        points = [bottomLeft, upperLeft, middle, upperRight, bottomRight]
        plan, factor = self.plan_cartesian_path(points)
        self.move_group.execute(plan, wait=True)
        self.liftPen()

    def drawJ(self, center, r):
        """ Draws an J using cartesian path of points, ends with pen up
        using a pose goal
        """
        # Define positions for each point of the J
        points = self.curve_poses(center, r, 180, 360, 10)
        points.append((points[-1][0], points[-1][1], points[-1][2] + .2))
        points.append((points[-1][0] - .15, points[-1][1], points[-1][2]))
        points.append((points[-1][0] + .3, points[-1][1], points[-1][2]))
        cartesian_plan, fraction = self.plan_cartesian_path(points)
        self.move_group.execute(cartesian_plan, wait=True)
        self.liftPen()

    def drawO(self, center, r):
        """ Draws an O using a cartesian path of points
        from circle equation
        """
        points = self.curve_poses(center, r, 0, 360, 10)
        cartesian_plan, fraction = self.plan_cartesian_path(points)
        self.move_group.execute(cartesian_plan, wait=True)

    def liftPen(self):
        """ Moves the pen up  to prepare for the next letter
        and to prevent the pen from dragging across the board
        """

        pos = self.move_group.get_current_pose().pose.position
        self.go_to_pose_goal((pos.x, pos.y - .15, pos.z), None)


def main():
    """ Starting establishes robot, puts it in starting state with joint goals,
    and then uses pose goals to draw the initial M
    """

    print("============ Beginning Motion ============")
    drawer = DrawingRobot()
    drawer.startPosition()
    # drawer.startPosition()
    # time.sleep(10)
    # drawer.startPosition()
    print("============ At Starting Position ============")
    drawer.drawM((-.6, drawer.boardDistance, 0.2))
    print("============ M Drawn ============")
    drawer.drawJ((-.1, .3), .1)
    print("============ J Drawn ============")
    drawer.drawO((.45, .35), .15)
    print("============ O Drawn ============")
    drawer.startPosition()
    print("============ At Ending Position ============")


if __name__ == "__main__":
    main()
